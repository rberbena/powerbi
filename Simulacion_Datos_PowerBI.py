#Creado por Rubén Berbena para DEMO de streaming de datos en Power BI para Empower Data 11/07/2018

#Aqui definimos las librerias y las clases a usar.
import random
import time
import sys
import datetime
import json
import urllib.request
import requests
from urllib.parse import urlparse

#Aquí va el link de conexión con la API de Power BI
urlpower = "https://api.powerbi.com/beta/01cc96ed-7eeb-42f2-bba2-6ec3c6d565f8/datasets/7857ea36-75e0-47fb-8c33-628ce875d2eb/rows?key=SIj7qzrXxmaUk6HI0zkygK9QF0jO3eUnQPsf2ZhEorNuog1SO8Ve48MwQdXcYss0IdT2KhBpUPzAO5neDK0wRQ%3D%3D"
    
#Variables de telemetria
#inserte variables para simular aqui:
def tiempo():
    time.sleep(3)
#Este método simula los valores de telemetria aleatorios y los concatena en el mensaje a enviar a Power BI cada 3 segundos dependiendo del time.sleep
def envio():
    while True:
        contador = 0
        tiempo()
        if contador < 3:
            tiempo_cadena = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            randx = random.randint(1, 255) #Generamos números random (int) del 1 al 255
            randy  = random.randint(1, 255)
            randz = random.randint(1, 255)
            randbol  = random.randint(0,1)
            randlong = random.randint(1, 255)
            randlat = random.randint(2, 255)
           
        envioPowerBi = {"time":tiempo_cadena,"x":randx,"y":randy,"z":randz,"bolsa":randbol,"long":randlong,"lat":randlat} #Aqui se forma la cadena de datos random que se enviarán a Power BI
        print(envioPowerBi)
        print("Dato enviado")
            
        #Aqui se mandan los datos a Power BI  
        headers = {'content-type': 'application/json'}
        response = requests.post(url=urlpower, data=json.dumps(envioPowerBi), headers=headers)
          
envio() #Ejecutamos el método de envío.
            
